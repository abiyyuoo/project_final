# Final Project

## Kelompok 5

-   GALIH S OKTAVIANTO (@uselessahar)
-   Samuel (@samuelsih)
-   Alfarizqi Abiyyu (@alfarizqi_abiyyu)

## Tema Project

### Musikpedia penyedia layanan rating untuk musik

## ERD

![](/public/image/erd.png)

##Link Video
Link Demo Aplikasi : https://youtu.be/RXJOQ7B8u1o
Link Deploy : http://final-project-laravel-sanber.herokuapp.com/
